### Install
```bash
npm install
```

### Run React
```bash
npm start
```

### Install json-server
```bash
npm install -g json-server
```

### Run json-server
```bash
json-server --watch db.json --port=4000
```

