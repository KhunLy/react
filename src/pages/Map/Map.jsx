import { LayerGroup, LayersControl, MapContainer, Marker, Popup } from 'react-leaflet';
import './Map.scss';
import BELGIUM_BORDERS from './borders/border-belgium.json';
import HAINAUT_BORDERS from './borders/border-hainaut.json';
import NAMUR_BORDERS from './borders/border-namur.json';
import LIEGE_BORDERS from './borders/border-liege.json';
import BRABANT_WALLON_BORDERS from './borders/border-brabant-w.json';
import LUXEMBOURG_BORDERS from './borders/border-luxembourg.json';
import 'leaflet-boundary-canvas/src/BoundaryCanvas.js';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import * as L from 'leaflet';
import { useEffect, useRef, useState } from 'react';
import { nanoid } from '@reduxjs/toolkit';
import axios from 'axios';

const POSITION_CLASSES = {
    bottomleft: 'leaflet-bottom leaflet-left',
    bottomright: 'leaflet-bottom leaflet-right',
    topleft: 'leaflet-top leaflet-left',
    topright: 'leaflet-top leaflet-right',
}

const BELGIUM_BOUNDS = [[51.50, 6.42],[49.5, 2.55]];

const MAP_URL = 'http://a.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png';

const COUNTIES_BORDERS = [
    NAMUR_BORDERS,
    BRABANT_WALLON_BORDERS,
    LIEGE_BORDERS,
    LUXEMBOURG_BORDERS,
    HAINAUT_BORDERS,
];

const DEFAULT_LAYER_STYLE = ({
    color: 'green',
    opacity: 0.2,
    fillColor: 'transparent',
});

const HOVER_LAYER_STYLE = ({
    ...DEFAULT_LAYER_STYLE,
    opacity: 0.5,
});

const SELECTED_LAYER_STYLE = ({
    ...HOVER_LAYER_STYLE,
    opacity: 1,
});

const FAKE_DATAS = [
    { latitude: 50.5, longitude: 4.48, titre: 'foret 1', tag: 'Forêt'  },
    { latitude: 50.45, longitude: 4.49, titre: 'foret 2', tag: 'Forêt'  },
    { latitude: 50.56, longitude: 4.53, titre: 'champs 1', tag: 'Champs'  },
    { latitude: 50.48, longitude: 4.49, titre: 'champs 2', tag: 'Champs'  },
];


const ZoomOutControl = ({ position, onClick }) => {
    const positionClass = (position && POSITION_CLASSES[position]) || POSITION_CLASSES.topleft;
    return (
        <div className={positionClass}>
          <button onClick={onClick} className="leaflet-control leaflet-control-back"><ArrowBackIcon /></button>
        </div>
    );
}

const PCMarker = props => {
    const { latitude, longitude, titre } = props
    return (<Marker position={[latitude, longitude]}>
        <Popup>{titre}</Popup>
    </Marker>);
}

const Map = () => {
    const mapRef = useRef(null);
    const mapLayerRef = useRef(null);
    const selectedLayerRef = useRef(null);
    const [zoomIn, setZoomIn] = useState(false);
    const [markersGroups, setMarkersGroups] = useState({});

    const groupByTag = markers => {
        return markers.reduce((r,m) => {
            if(!r[m.tag]) {
                r[m.tag] = [];
            }
            r[m.tag] = [...r[m.tag], m];
            return r;
        }, {});
    }

    useEffect(() => {
        axios.get(process.env.REACT_APP_API_URL + '/markers').then(({data}) => {
            setMarkersGroups(groupByTag(data));
        }).catch(() => {
            // dev debug
            setMarkersGroups(groupByTag(FAKE_DATAS));
        });
    }, []);

    const onEachFeature = borders => (feature, layer) => {
        layer.on({
            click: () => {
                if(selectedLayerRef.current === layer)
                    return;
                changeLayer(borders);
                selectedLayerRef.current?.setStyle(DEFAULT_LAYER_STYLE);
                selectedLayerRef.current = layer;
                setZoomIn(true);
                layer.setStyle(SELECTED_LAYER_STYLE);
                mapRef.current.fitBounds(layer.getBounds());
            }, 
            mouseover: () => {
                if(selectedLayerRef.current === layer)
                    return;
                layer.setStyle(HOVER_LAYER_STYLE);
            },
            mouseout: () => {
                if(selectedLayerRef.current === layer)
                    return;
                layer.setStyle(DEFAULT_LAYER_STYLE);
            }
        });
    }

    const changeLayer = (borders) => {
        mapLayerRef.current.removeFrom(mapRef.current);
        mapLayerRef.current = L.TileLayer.boundaryCanvas(MAP_URL, {
            boundary: borders
        }).addTo(mapRef.current);
        mapLayerRef.current.addTo(mapRef.current);
    }

    const zoomOut = () => {
        changeLayer(BELGIUM_BORDERS);
        selectedLayerRef.current?.setStyle(DEFAULT_LAYER_STYLE);
        selectedLayerRef.current = null;
        setZoomIn(false);
        mapRef.current.fitBounds(BELGIUM_BOUNDS);
    }

    const initMap = map => {
        mapRef.current = map;
        initBelgium();
        initCounties();
    };

    const initBelgium = () => {
        mapLayerRef.current = L.TileLayer.boundaryCanvas(MAP_URL, {
            boundary: BELGIUM_BORDERS
        }).addTo(mapRef.current);
    };

    const initCounties = () => {
        for(let borders of COUNTIES_BORDERS) {
            L.geoJSON(borders, {
                style: DEFAULT_LAYER_STYLE,
                onEachFeature: onEachFeature(borders),
            }).addTo(mapRef.current);
        }
    };

    return (
        <>
        <div className="map-container">
            <MapContainer scrollWheelZoom={false} 
                          zoom={false} 
                          zoomControl={false} 
                          bounds={BELGIUM_BOUNDS}
                          dragging={false}
                          doubleClickZoom={false} 
                          whenCreated={initMap}>

                { zoomIn && <ZoomOutControl position="topleft" onClick={zoomOut} /> }

                <LayersControl collapsed={false} position="bottomleft">
                    { Object.keys(markersGroups).map(key => 
                        <LayersControl.Overlay key={key} checked name={key}>
                            <LayerGroup>
                                {markersGroups[key].map(m => <PCMarker key={nanoid()} {...m} />)}
                            </LayerGroup>
                        </LayersControl.Overlay>
                    )}
                </LayersControl>
            </MapContainer>
        </div>
        </>
    );
};

export default Map;