import { Button, Card, TextField } from "@mui/material";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { sessionService } from "../../../services/sessionService";
import { start } from "../../../store/sessionSlice";
import './Login.scss';
import GoogleLogin from 'react-google-login';
import axios from "axios";
import { showToast } from '../../../store/interactionsSlice';

const Login = () => {

    const navigate = useNavigate();

    const dispatch = useDispatch();

    const login = () => {
        sessionService.login({ email: 'admin@be', password: 'admin' })
            .then(token => {
                dispatch(start(token));
                navigate('/');
            })
            .catch(error => {

            });
    }

    const onSuccesHandler = (data) => {
        console.log(data.tokenId);
        axios.post('http://localhost:56459/OauthLogin', {token: data.tokenId}).then(({data}) => {
            // set token in localstorage
            dispatch(showToast({severity: 'success', message: 'ok'}));
            //redirection
        }).catch(() => {
            dispatch(showToast({severity: 'error', message: 'KO'}));
        });
    }

    const onFailHandler = (e) => {
        dispatch(showToast({severity: 'error', message: 'KO'}));
    }

    return (
        <main className="auth">
            <Card variant="outlined">
                <form onSubmit={ login }>  
                    <div>
                        <TextField type="email" label="Email" variant="outlined" />
                    </div>
                    <div>
                        <TextField type="password" label="Mot de passe" variant="outlined" />
                    </div>
                    <div className="f-end">
                        <Button type="submit" color="primary">Se connecter</Button>
                    </div>
                </form>            
            </Card>
            <GoogleLogin 
                clientId="682462884108-qapggivi4gehggsm9aks4u0k3sqsj3mt.apps.googleusercontent.com"
                buttonText="Fucking Login"
                onSuccess={onSuccesHandler}
                onFailure={onFailHandler}
                cookiePolicy={'single_host_origin'}
            />
        </main>
    );
};

export default Login;