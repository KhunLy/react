import { useRoutes } from 'react-router';
import './App.scss';
import Routes from './Routes';
import DateAdapter from '@mui/lab/AdapterMoment';
import { LocalizationProvider } from '@mui/lab';
import { Alert, createTheme, Snackbar } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { hideToast } from './store/interactionsSlice';
import { ThemeProvider } from '@emotion/react';
import ConfirmDialog from './containers/ConfirmDialog/ConfirmDialog';
import ToastMessage from './containers/ToastMessage/toast-message';

const App = () => {

  const routes = useRoutes(Routes);



  const theme = createTheme({
    palette: {
      primary: {
        main: '#0b524f'
      }
    }
  });

  return (
    <ThemeProvider theme={theme}>
      <LocalizationProvider dateAdapter={DateAdapter}>
        {routes}
        <ConfirmDialog />
        <ToastMessage />
      </LocalizationProvider>
    </ThemeProvider>
  );
}

export default App;
