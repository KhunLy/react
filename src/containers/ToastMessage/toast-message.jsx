import { Alert, Snackbar } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { hideToast } from "../../store/interactionsSlice";


const ToastMessage = () => {

    const { open, severity, message } = useSelector(state => state.interactions.toast);

    const dispatch = useDispatch();

    return (
        <Snackbar open={open}
            autoHideDuration={6000}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            onClose={() => dispatch(hideToast())}>
            <Alert severity={severity} sx={{ width: '100%' }}>
                {message}
            </Alert>
        </Snackbar>
    );
};

export default ToastMessage;